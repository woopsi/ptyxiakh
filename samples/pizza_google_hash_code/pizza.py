import sys
import time
from ortools.sat.python import cp_model


def timer(solve_fun):
    def inner(file_path):
        print(f"Solving problem with data of {file_path}")
        # fetch the data from the file
        data, max_slices, total_pizzas = get_problem_data(file_path)
        # start the timer
        start = time.time()
        # solve
        solve_fun(data, max_slices, total_pizzas)
        # stop the timer
        end = time.time()
        print(f"Time elapsed: {end - start} \n")

    return inner


def get_problem_data(file_path):
    with open(file_path) as d_file:
        # problem data file content
        f_content = d_file.readlines()
        # first  line of the file holds the max slices
        # for the problem and the amount of the pizzas
        # of the problem
        problem_specifications = f_content[0].split()

        # this is the MAX limit
        max_slices = int(problem_specifications[0])

        # amount of pizzas of the problem
        number_of_pizzas = int(problem_specifications[1].rstrip())

        # a list of integers that represent the number of slices
        # that each pizzas has
        slices_per_pizza = [int(x) for x in f_content[1].split()]

    # ensure that the number of pizzas and the number of slices per pizza are correct
    if len(slices_per_pizza) != number_of_pizzas:
        print("number of slices per pizza and pizza number does not match")
        sys.exit()

    # create a collection of dicts
    # where each dict has the pizza_id (pizza counter)
    # and it's amount of slices
    data = []
    for index, slice_per_pizza in enumerate(slices_per_pizza, start=1):
        data.append({"pizza_id": f"pizza_{index}", "slices": slice_per_pizza})

    return data, max_slices, number_of_pizzas


def is_model_feasible_or_optimal(status):
    return status in [cp_model.FEASIBLE, cp_model.OPTIMAL]


@timer
def solve(data, max_slices, total_pizzas):
    my_model = cp_model.CpModel()
    solver = cp_model.CpSolver()

    # for each pizza, create a new int var
    # that has 2 possible values which is 0
    # or the total slices of the specific pizza.
    # which means that if the value is 0, the pizza is selected
    # and if not, it means that all the slices were added (pizza is selected)
    slices_intvars = [
        my_model.NewIntVarFromDomain(
            cp_model.Domain.FromValues([0, item['slices']]), item["pizza_id"]) for item in data
    ]

    # this is to show how many pizzas were selected
    # in the result
    pizzas_selected_counter = 0

    # cant exceed the max limit of the problem
    my_model.Add(sum(slices_intvars) <= max_slices)

    # we want to get as much slices as possible
    my_model.Maximize(sum(slices_intvars))

    status = solver.Solve(my_model)

    if is_model_feasible_or_optimal(status):
        sum_slices = 0
        for _v in slices_intvars:
            if solver.Value(_v) != 0:
                sum_slices += solver.Value(_v)
                pizzas_selected_counter += 1

        print(f"Used {pizzas_selected_counter} of {total_pizzas} pizzas")
        print(f"Max Slices: {max_slices} \nSum Slices: {sum_slices}")
        print(f"Solution is {solver.StatusName(status)}")


if __name__ == "__main__":
    # problem file paths
    f_paths = (
        "./data/a_example.in",
        "./data/b_small.in",
        "./data/c_medium.in",
        "./data/d_quite_big.in",
        "./data/e_also_big.in",
    )
    for fp in f_paths:
        solve(fp)
