from ortools.sat.python import cp_model


def solve_magic_square(n=2):
    my_model = cp_model.CpModel()
    solver = cp_model.CpSolver()

    lower_bound = 1
    max_bound = n**2

    column_and_row_sum = n * (n ** 2 + 1) // 2

    variables_list = []
    _variables = []

    for i in range(n):
        cols = []
        for j in range(n):
            # create a variable representing a value in a column
            var = my_model.NewIntVar(lower_bound, max_bound, f"row_{i}col_{j}")
            # add the variable to a list which represents a column
            cols.append(var)
            # add the variable to the list with all the variables
            variables_list.append(var)
        _variables.append(cols)

    for i, row in enumerate(_variables):
        # each row must have sum == column_and_row_sum
        my_model.Add(sum(row) == column_and_row_sum)
        _cc = [] # this is the column for each row

        for j, col in enumerate(row):
            _cc.append(_variables[j][i])

        # same for colors
        my_model.Add(sum(_cc) == column_and_row_sum)

    my_model.AddAllDifferent(variables_list)

    status = solver.Solve(my_model)

    print(solver.StatusName(status))

    if solver.StatusName(status) == "FEASIBLE":
        for row in _variables:
            for col in row:
                print("{:<4}".format(solver.Value(col)), end="")
            print()
