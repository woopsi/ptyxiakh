from ortools.sat.python import cp_model


class SolutionPrinter(cp_model.CpSolverSolutionCallback):
    def __init__(self, weights, volumes):
        cp_model.CpSolverSolutionCallback.__init__(self)
        self.__weights = weights
        self.__volumes = volumes
        self.__solutions_count = 0

    def OnSolutionCallback(self):
        self.__solutions_count += 1
        # print(self.__volumes, self.__weights)

    @property
    def solutions_count(self):
        return self.__solutions_count


def solve_ntrucks_nboxes(model):
    # num_of_trucks = 5
    # box_weights = [140, 200, 450, 700, 120, 300, 250, 125, 600, 650]
    # box_volumes = [50, 20, 45, 15, 60, 40, 40, 20, 10, 50]
    # max_truck_weight = [1500, 1450, 700, 900, 1100]
    # max_truck_volume = [150, 80, 100, 200, 150]
    num_of_trucks = 1
    box_volumes = [5, 10, 15]
    box_weights = [2, 2, 1]

    max_truck_weight = [10]

    max_truck_volume = [25]

    _box_weights = []
    _box_volumes = []

    for truck in range(num_of_trucks):
        # convert the lists to IntVars
        _box_weights = [
            model.NewIntVar(0, weight, str(weight))
            for weight in box_weights
            ]

        _box_volumes = [
            model.NewIntVar(0, volume, str(volume))
            for volume in box_volumes
        ]

        model.AddSumConstraint(_box_weights, 0, max_truck_weight[truck])
        model.AddSumConstraint(_box_volumes, 0, max_truck_volume[truck])
        model.AddAllDifferent(_box_volumes)
        model.AddAllDifferent(_box_weights)

    solver = cp_model.CpSolver()
    status = solver.Solve(model)
    solution_printer = SolutionPrinter(_box_weights, _box_volumes)

    solver.SearchForAllSolutions(model, solution_printer)

    print(solver.StatusName(status))

    for weight, volume in zip(_box_weights, _box_volumes):
        print(f"Optimal weight {solver.Value(weight)} \
                Optimal volume {solver.Value(volume)}")

    # print(solution_printer.solutions_count, solver.StatusName(status))


if __name__ == '__main__':
    model = cp_model.CpModel()
    solve_ntrucks(model)
