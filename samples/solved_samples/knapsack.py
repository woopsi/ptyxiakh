from ortools.sat.python import cp_model

MAX_WEIGHT = 15


def solve_knapsack(problem_data):
    my_model = cp_model.CpModel()
    solver = cp_model.CpSolver()

    allowed_weights = [item['weight'] for item in problem_data]
    allowed_values = [item['value'] for item in problem_data]
    #
    weights = [my_model.NewIntVar(0, 1000, str(index)) for index in range(len(problem_data))]
    values = [my_model.NewIntVar(0, 1000, str(index)) for index in range(len(problem_data))]

    my_model.AddAllowedAssignments(weights, [allowed_weights])
    my_model.AddAllowedAssignments(values, [allowed_values])
    #
    # print(weights)

    sum_value = sum(values)
    sum_weight = sum(weights)
    #
    my_model.Maximize(sum_value)
    my_model.Minimize(sum_weight)

    my_model.AddMaxEquality()

    status = solver.Solve(my_model)

    print(solver.StatusName(status))
    #
    print(solver.Value(sum_value), solver.Value(sum_weight))
