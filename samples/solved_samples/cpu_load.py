from ortools.sat.python import cp_model

MAX_CPU_LOAD = 100


def solve_cpu_load(problem_data):
    my_model = cp_model.CpModel()
    solver = cp_model.CpSolver()

    task_vars = []
    var_dict = {}

    makespan = my_model.NewIntVar(0, 1000, 'makespan')
    makespans_list = []

    # get all the durations and loads in a list
    durations_list = [data['duration'] for job, data in problem_data.items()]
    loads_list = [data['load'] for job, data in problem_data.items()]

    # for each cpu job, create a new interval variable with the duration given in problem data
    # and add it's new start and and time in a dictionary which will be used to print the
    # optimal values if there are any.
    # At the same time, for each job, add to a list it's load so we can minimize the load
    # on the cpu
    for cpu_job, cpu_data in problem_data.items():
        start_task_time = my_model.NewIntVar(0, max(durations_list), 'start_time')
        end_task_time = my_model.NewIntVar(0, 1000, 'end_time')

        task_vars.append(my_model.NewIntervalVar(start_task_time, cpu_data['duration'], end_task_time, cpu_job))

        var_dict[cpu_job] = {
            "start": start_task_time,
            "end": end_task_time
        }
        makespans_list.append(cpu_data['load'])

    my_model.AddMaxEquality(makespan, [makespan for makespan in makespans_list])

    my_model.AddCumulative(task_vars, loads_list, MAX_CPU_LOAD)

    my_model.Minimize(makespan)

    status = solver.Solve(my_model)

    print(solver.StatusName(status))

    if status == cp_model.OPTIMAL:
        for task in task_vars:
            start = solver.Value(var_dict[task.Name()]["start"])
            end = solver.Value(var_dict[task.Name()]["end"])

            print(f"Job {task.Name()} Start {start} End {end}")


if __name__ == '__main__':
    solve_cpu_load()
