from ortools.sat.python import cp_model

BOARD_SIZE = 5


class QueensSolutionPrinter(cp_model.ObjectiveSolutionPrinter):
    def __init__(self, variables):
        cp_model.CpSolverSolutionCallback.__init__(self)
        self.__variables = variables
        self.__solution_count = 0

    def OnSolutionCallback(self):
        self.__solution_count += 1
        # for v in self.__variables:
        # print('%s = %i' % (v, self.Value(v)), end = ' ')

    @property
    def solution_count(self):
        return self.__solution_count


def solve_queens():
    my_model = cp_model.CpModel()
    queens = [
        my_model.NewIntVar(0, BOARD_SIZE - 1, str(queen))
        for queen in range(BOARD_SIZE)
    ]

    my_model.AddAllDifferent(queens)

    for i in range(BOARD_SIZE):
        diagonal_1 = []
        diagonal_2 = []
        for j in range(BOARD_SIZE):
            queen_1 = my_model.NewIntVar(0, 2 * BOARD_SIZE, f'diag1_{i}')
            queen_2 = my_model.NewIntVar(-BOARD_SIZE, BOARD_SIZE, f'diag2_{i}')

            diagonal_1.append(queen_1)
            diagonal_2.append(queen_2)

            my_model.Add(queen_1 == queens[j] + j)
            my_model.Add(queen_2 == queens[j] - j)

    my_model.AddAllDifferent(diagonal_1)
    my_model.AddAllDifferent(diagonal_2)

    sp = QueensSolutionPrinter(queens)
    solver = cp_model.CpSolver()
    solve_result_status = solver.Solve(my_model)

    if solver.StatusName(solve_result_status) == 'FEASIBLE':
        print(f"{sp.solution_count} feasible solutions found")
    else:
        print("No feasible solutions found")

