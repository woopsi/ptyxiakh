# Some common to  all utilities.
import json
from functools import reduce

def loadFile(filename):
    with open(filename, 'r') as f:
        datastore = json.load(f)
    return datastore

def load_rabbit_config():
    return loadFile('./rabbitmq_config.json')


def fetch(adict,info):
    try:
        info.insert(0,adict)
        return reduce( (lambda x, y: x.get(y)) ,info)
    except:
         return None

## For testing
if __name__ == "__main__":
    d = load_rabbit_config()
    print(json.dumps(d))
